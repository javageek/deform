package com.jjxliu.deform.action;

import com.jjxliu.deform.model.ResultModel;
import com.jjxliu.deform.pojo.UeEntry;
import com.jjxliu.deform.pojo.UeForm;
import com.jjxliu.deform.service.UeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author user
 */
@Controller
@RequestMapping("/form")
public class FormAction {

    @RequestMapping("/index")
    public String index() {
        return null;
    }

    @RequestMapping("/page")
    public ModelAndView page(String view) {
        ModelAndView v = new ModelAndView(view);

        return v;
    }

    @Autowired
    private UeService ueService;

    @RequestMapping("/edit")
    public ModelAndView editForm(Integer form_id) {
        ModelAndView view = new ModelAndView("/form/form_edit");

        if (form_id != null) {
            UeForm form = ueService.findByFormId(form_id);
            view.addObject("form_name", form.getForm_name());
            view.addObject("form_id", form.getForm_id());
            view.addObject("form_parse", form.getParse());
            view.addObject("form_data", form.getData());
            view.addObject("form_template", form.getTemplate());
            view.addObject("form_html", form.getHtml());
        }

        return view;
    }

    @RequestMapping("/previewEditForm")
    public ModelAndView previewEditForm(String formeditor, Integer form_id) {
        ModelAndView model = new ModelAndView();

        UeForm form = null;
        if (formeditor != null) {
            form = UeForm.praseTemplate(formeditor);
        } else if (form_id != null && form_id > 0) {
            form = ueService.findByFormId(form_id);
        }

        String parse = form.getParse();
        String template = form.getTemplate();

        model.addObject("parse", parse);
        model.addObject("template", template);
        model.addObject("html", form.getHtml());

        return model;
    }

    //previewEntry
    @RequestMapping("/previewEntry")
    public ModelAndView previewEntry(Integer entry_id) {
        ModelAndView model = new ModelAndView();

        UeEntry entry = ueService.findById(entry_id);


        UeForm form = ueService.findByFormId(entry.getForm_id());


        String parse = form.getView(entry.getValue());
        String html = form.getEdit(entry.getValue());

        model.addObject("parse", parse);

        model.addObject("html", html);

        return model;
    }

    @RequestMapping("/removeForm")
    @ResponseBody
    public ResultModel removeForm(@RequestParam(value = "forms[]") Integer[] forms) {
        ResultModel model = new ResultModel();

        for (int form_id : forms) {
            ueService.removeForm(form_id);
        }


        return model;
    }

    @RequestMapping("/removeEntry")
    @ResponseBody
    public ResultModel removeEntry(@RequestParam(value = "entrys[]") Integer[] entrys) {
        ResultModel model = new ResultModel();

        for (int id : entrys) {
            ueService.removeEntry(id);
        }


        return model;
    }

    @RequestMapping("/saveOrUpdateForm")
    @ResponseBody
    public ResultModel saveForm(String template, String form_name, Integer form_id) {
        ResultModel model = new ResultModel();

        //System.out.println("parse_form : " + parse_form);

        if (template == null || template.trim().length() < 1) {
            model.setMessage("请至少绘制一个元素");
            return model;
        }

        UeForm form = UeForm.praseTemplate(template);
        form.setForm_name(form_name);
        form.setForm_id(form_id);

        String error = form.getForm_id() != null && form.getForm_id() > 0 ? ueService.updateForm(form) : ueService.addForm(form);

        if (error != null) {
            model.setMessage(error);
            return model;
        }

        //包含 template data parse  fields add_fields
        model.putData("data", form);


        return model;
    }

    @RequestMapping("/formList")
    @ResponseBody
    public List<UeForm> forms() {

        return ueService.findAllForms();
    }

    @RequestMapping("/entryList")
    @ResponseBody
    public List<UeEntry> entrys() {
        return ueService.findAllEntry();
    }

    @RequestMapping("/entry")
    @ResponseBody
    public UeEntry entry(Integer id) {

        return ueService.findById(id);
    }

    @RequestMapping("/start_edit")
    public ModelAndView startEntry(int form_id) {
        ModelAndView view = new ModelAndView("/form/entry_edit");
        view.addObject("form_id", form_id);

        UeForm form = ueService.findByFormId(form_id);

        String edit = form.getHtml();

        view.addObject("edit", edit);

        return view;
    }

    @RequestMapping("/entry_edit")

    public ModelAndView editEntry(int entry_id) {
        ModelAndView view = new ModelAndView();

        UeEntry e = ueService.findById(entry_id);

        view.addObject("id", e.getId());
        view.addObject("form_id", e.getForm_id());

        UeForm form = ueService.findByFormId(e.getForm_id());

        String edit = form.getEdit(e.getValue());

        view.addObject("edit", edit);

        return view;

    }

    @RequestMapping("/saveOrUpdateEntry")
    @ResponseBody
    public ResultModel saveEntry(UeEntry entry) {
        ResultModel model = new ResultModel();

        //System.out.println("parse_form : " + parse_form);
        Integer form_id = entry.getForm_id();

        if (form_id == null || form_id < 1) {
            model.setMessage("请选择需要的动态表单");
            return model;
        }


        String error = entry.getId() != null && entry.getId() > 0 ? ueService.updateEntry(entry) : ueService.addEntry(entry);

        if (error != null) {
            model.setMessage(error);
            return model;
        }

        //包含 template data parse  fields add_fields
        model.putData("data", entry);


        return model;
    }


    @RequestMapping("/demo_datas")
    @ResponseBody
    public List<String> demoDatas() {

        List list = new ArrayList<String>();
        for (int i = 0; i < 10; i++) {
            list.add("name" + i);
        }

        return list;

    }

}
