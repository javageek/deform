package com.jjxliu.deform.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.jjxliu.deform.pojo.UeForm;

@Repository
public interface UeFormDao {

	@Insert("insert into form(form_name,template,data,parse,fields,html,crtime,modify_time) values(#{e.form_name},#{e.template},#{e.data},#{e.parse},#{e.fields},#{e.html},#{e.crtime},#{e.modify_time})")
	@Options(useGeneratedKeys=true,keyProperty="e.form_id",keyColumn="form_id")
	public void insert(@Param("e") UeForm e);
	
	@Select("select * from form")
	public List<UeForm> findAll();
	
	@Select("select form_id,form_name from form where form_name=#{form_name}")
	public UeForm findByName(@Param("form_name") String form_name);
	
	@Select("select * from form where form_id = #{form_id}")
	public UeForm findById(@Param("form_id") int form_id);
	
	@Delete("delete from form where form_id = #{form_id} ")
	public void delete(@Param("form_id") int form_id);
	
	@Update("update form set form_name=#{e.form_name} ,html=#{e.html}, template=#{e.template} , data=#{e.data} , parse=#{e.parse}, fields=#{e.fields} where form_id=#{e.form_id}")
	public void update(@Param("e") UeForm e);
	
}
